#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define GL3_PROTOTYPES 1
#include <GLES3/gl3.h>
#include <SDL2/SDL.h>
#define FALSE 0

/* The opengl and window code used is based on example template code given in
 * https://www.khronos.org/opengl/wiki/Tutorial1:_Creating_a_Cross_Platform_OpenGL_3.2_Context_in_SDL_(C_/_SDL)
 * So I started with this working example for the graphics code and modified it to my needs.
 */

char frag[] =
		"#version 330 core \n\
in vec2 UV; \
out vec3 color; \
uniform sampler2D texturedata; \
void main(){ \
     color = texture( texturedata, UV ).rgb; \
}";

char vert[] =
		"#version 150 \n\
in  vec2 in_Position; \
out vec2 UV; \
void main(void) { \
    gl_Position = vec4(in_Position, 0.0, 1.0); \
    UV = vec2((in_Position.x + 1.0) / 2.0,(in_Position.y + 1.0) / 2.0); \
}";

#include <cuda.h>

// this macro checks for errors in cuda calls
#define Err(ans) { gpucheck((ans), __FILE__, __LINE__); }
inline void gpucheck(cudaError_t code, const char *file, int line) {
	if (code != cudaSuccess) {
		fprintf(stderr, "GPU Err: %s %s %d\n", cudaGetErrorString(code), file,
				line);
		exit(code);
	}
}

/* SoundSim - a simple 2D acoustic simulation for experimenting in stencil calculations.
 Eric McCreath GPL 2020 */

#define xdim 1024
#define ydim 1024
#define tsteps 4096
#define stencilpoints 6

float *values; // pressure values
float *stencilconstants;

int visualize = 0; // show the results along the way
int gpuapproach = 0; // 0 is just using the CPU 

unsigned char *data;
SDL_Window *mainwindow;
SDL_GLContext maincontext;
GLuint textureID;
GLuint shaderprogram;
int done = 0;
GLchar *vertexsource, *fragmentsource;
GLuint vertexshader, fragmentshader;
GLuint vao, vbo[1];


void sdldie(const char *msg) {
	printf("%s: %s\n", msg, SDL_GetError());
	SDL_Quit();
	exit(1);
}

void checkSDLError(int line = -1) {
#ifndef NDEBUG
	const char *error = SDL_GetError();
	if (*error != '\0') {
		printf("SDL Error: %s\n", error);
		if (line != -1)
			printf(" + line: %i\n", line);
		SDL_ClearError();
	}
#endif
}

void setupgraphics() {
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		sdldie("problem setting up SDL");
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	mainwindow = SDL_CreateWindow("Sound Sim", SDL_WINDOWPOS_CENTERED,
	SDL_WINDOWPOS_CENTERED, 1024, 1024, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!mainwindow)
		sdldie("Unable to create window");
	checkSDLError(__LINE__);
	maincontext = SDL_GL_CreateContext(mainwindow);
	checkSDLError(__LINE__);
	SDL_GL_SetSwapInterval(1);

	int IsCompiled_VS, IsCompiled_FS;
	int IsLinked;
	int maxLength;
	char *vertexInfoLog;
	char *fragmentInfoLog;
	char *shaderProgramInfoLog;

	const GLfloat diamond[8][2] = { { -1.0, -1.0 }, { 1.0, -1.0 }, { 1.0, 1.0 },
			{ -1.0, -1.0 }, { -1.0, 1.0 }, { 1.0, 1.0 } };

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), diamond,
	GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	vertexsource = vert;
	fragmentsource = frag;

	vertexshader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexshader, 1, (const GLchar**) &vertexsource, 0);
	glCompileShader(vertexshader);
	glGetShaderiv(vertexshader, GL_COMPILE_STATUS, &IsCompiled_VS);
	if (IsCompiled_VS == FALSE) {
		glGetShaderiv(vertexshader, GL_INFO_LOG_LENGTH, &maxLength);
		vertexInfoLog = (char *) malloc(maxLength);
		glGetShaderInfoLog(vertexshader, maxLength, &maxLength, vertexInfoLog);
		printf("error : %s\n", vertexInfoLog);
		free(vertexInfoLog);
		exit(1);
	}

	fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentshader, 1, (const GLchar**) &fragmentsource, 0);
	glCompileShader(fragmentshader);
	glGetShaderiv(fragmentshader, GL_COMPILE_STATUS, &IsCompiled_FS);
	if (IsCompiled_FS == FALSE) {
		glGetShaderiv(fragmentshader, GL_INFO_LOG_LENGTH, &maxLength);
		fragmentInfoLog = (char *) malloc(maxLength);

		glGetShaderInfoLog(fragmentshader, maxLength, &maxLength,
				fragmentInfoLog);
		printf("error : %s\n", fragmentInfoLog);
		free(fragmentInfoLog);
		exit(1);
	}

	shaderprogram = glCreateProgram();
	glAttachShader(shaderprogram, vertexshader);
	glAttachShader(shaderprogram, fragmentshader);
	glBindAttribLocation(shaderprogram, 0, "in_Position");
	glLinkProgram(shaderprogram);
	glGetProgramiv(shaderprogram, GL_LINK_STATUS, (int *) &IsLinked);
	if (IsLinked == FALSE) {
		glGetProgramiv(shaderprogram, GL_INFO_LOG_LENGTH, &maxLength);
		shaderProgramInfoLog = (char *) malloc(maxLength);
		glGetProgramInfoLog(shaderprogram, maxLength, &maxLength,
				shaderProgramInfoLog);
		printf("error : %s\n", shaderProgramInfoLog);
		free(shaderProgramInfoLog);
		exit(1);
	}
	glUseProgram(shaderprogram);

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	data = (unsigned char *) malloc(xdim * ydim * 3);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	int texLoc = glGetUniformLocation(shaderprogram, "texturedata");
	glUniform1i(texLoc, 0);
	glActiveTexture(GL_TEXTURE0);
}

void shutdowngraphics() {
	glUseProgram(0);
	glDisableVertexAttribArray(0);
	glDetachShader(shaderprogram, vertexshader);
	glDetachShader(shaderprogram, fragmentshader);
	glDeleteProgram(shaderprogram);
	glDeleteShader(vertexshader);
	glDeleteShader(fragmentshader);
	glDeleteBuffers(1, vbo);
	glDeleteVertexArrays(1, &vao);
	SDL_GL_DeleteContext(maincontext);
	SDL_DestroyWindow(mainwindow);
	SDL_Quit();
}

void drawscreen(int current) {
	int vi, i, j;
	float vv;
	float *cvalue;
	cvalue = &values[current * xdim * ydim];
	for (i = 0; i < xdim; i++) {
		for (j = 0; j < ydim; j++) {
			vv = cvalue[j * xdim + i] / 300.0;
			//	if (vv < 1.0) vv = -1.0;
			//	if (vv > 1.0) vv = 1.0;
			vi = (((vv + 1.0) / 2.0) * 255.0);
			data[3 * (j * xdim + i)] = vi;
			data[3 * (j * xdim + i) + 1] = vi;
			data[3 * (j * xdim + i) + 2] = vi;
		}
	}

	glClear(GL_COLOR_BUFFER_BIT);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, xdim, ydim, 0, GL_RGB,
	GL_UNSIGNED_BYTE, data);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	SDL_GL_SwapWindow(mainwindow);

	SDL_Event events;
	SDL_PollEvent(&events);
	if (events.type == SDL_QUIT) {
		done = 1;
	}
}

void setconstants(float *constants, float c) {
	float dx = 0.01;
	float dt = 0.00001;
	float t2c2ONx2 = dt * dt * c * c / (dx * dx);
	constants[0] = 2 - 4 * t2c2ONx2;
	constants[1] = -1;
	constants[2] = t2c2ONx2;
	constants[3] = t2c2ONx2;
	constants[4] = t2c2ONx2;
	constants[5] = t2c2ONx2;
}

void initializeMemory() {
	int x, y, t;

	Err(cudaMallocManaged( &values, 3 * xdim * ydim * sizeof(float)));
	for (t = 0; t < 3; t++) {
		for (x = 0; x < xdim; x++) {
			for (y = 0; y < ydim; y++) {
				values[t * (xdim * ydim) + y * xdim + x] = 0.0f;
			}
		}
	}

	Err(cudaMallocManaged( &stencilconstants, stencilpoints * sizeof(float)));
	setconstants(stencilconstants, 320.0f);
}



void updateStencilCPU(int current, int prev, int prevprev) {
	int i, j;
	float *c, *cvalue, *pvalue, *ppvalue;
	c = stencilconstants;
	cvalue = &values[current * xdim * ydim];
	pvalue = &values[prev * xdim * ydim];
	ppvalue = &values[prevprev * xdim * ydim];
	for (j = 1; j < ydim - 1; j++) {
		for (i = 1; i < xdim - 1; i++) {
			cvalue[j * xdim + i] = c[0] * pvalue[j * xdim + i] +
					       c[1] * ppvalue[j * xdim + i] +
					       c[2] * pvalue[(j + 1) * xdim + i] +
					       c[3] * pvalue[(j - 1) * xdim + i] +
					       c[4] * pvalue[j * xdim + i + 1] +
					       c[5] * pvalue[j * xdim + i - 1];
		}
	}
}

__global__ void stencilV1(float *cvalue, float *pvalue, float *ppvalue,
		float *stencilconstants) {
	float *c;
	int i = threadIdx.x;
	int j = blockIdx.x;
   	c = stencilconstants;
	if (i > 0 && i < xdim - 1 && j > 0 && j < ydim - 1) {
		cvalue[j * xdim + i] = c[0] * pvalue[j * xdim + i] +
					c[1] * ppvalue[j * xdim + i] +
					c[2] * pvalue[(j + 1) * xdim + i] +
					c[3] * pvalue[(j - 1) * xdim + i] +
					c[4] * pvalue[j * xdim + i + 1] +
					c[5] * pvalue[j * xdim + i - 1];
	}
}

void updateStencilGPUV1(int current, int prev, int prevprev) {
	float *cvalue;
	float *pvalue;
	float *ppvalue;
	cvalue = &values[current * xdim * ydim];
	pvalue = &values[prev * xdim * ydim];
	ppvalue = &values[prevprev * xdim * ydim];

	stencilV1<<<1024, 1024>>>(cvalue, pvalue, ppvalue, stencilconstants);
        Err(cudaDeviceSynchronize());
}

void injectSound(int current, int step) {

	float *cvalue;

	cvalue = &values[current * xdim * ydim];

	if (step < 1200.0) {
		cvalue[235 * xdim + 130] = 300.0 * sin(3.14 * (step / 100.0));
		cvalue[120 * xdim + 400] = 300.0 * sin(3.14 * (step / 200.0));
	}
	if (step > 200 && step < 1400.0) {
		cvalue[435 * xdim + 130] = 300.0 * sin(3.14 * ((step - 200) / 200.0));
	}

}

void runSteps() {

	int step;
	for (step = 0; step < tsteps && !done; step++) {
                if (gpuapproach == 0) {
			updateStencilCPU(step % 3, (step + 2) % 3, (step + 1) % 3);
		} else if (gpuapproach == 1) {
  
			updateStencilGPUV1(step % 3, (step + 2) % 3, (step + 1) % 3);
		}
		injectSound(step % 3, step);

		if (visualize)
			drawscreen(step % 3);
	}
}

void usage() {
   printf("soundsim [options] \n");
   printf("Options: \n");
   printf("   -h : list options\n");
   printf("   -v : enable the visualization \n");
   printf("   -gpu NUMBER : run the simulation using the gpu using\n");
   printf("                 optimization NUMBER, start at 1 \n");
}

int main(int argc, char *argv[]) {

	int pos = 1;
	while (pos < argc) {
		if (strcmp("-h", argv[pos]) == 0) {
			usage();
			exit(0);
		} if (strcmp("-v", argv[pos]) == 0) {
			visualize = 1;
		} else if (strcmp("-gpu", argv[pos]) == 0) {
			pos++;
                        if (pos < argc)
                        	gpuapproach = strtol(argv[pos], NULL, 10);
		}
		pos++;
	}

        printf("running with gpuapproach : %d\n",gpuapproach);

	initializeMemory();
	if (visualize)
		setupgraphics();

	runSteps();

	if (visualize)
		shutdowngraphics();
	return 0;
}
