FROM rapidsai/rapidsai:0.10-cuda9.2-runtime-ubuntu16.04

# CuPy is used for GPU-Accelerated UDFs
RUN conda run -n rapids pip install cupy-cuda92

# Update to latest jupyterlab and rebuild modules
#RUN conda run -n rapids pip install --upgrade jupyterlab

#RUN conda run -n rapids pip install ipycanvas
#RUN conda run -n rapids pip install ipywidgets
#RUN conda run -n rapids pip install nodejs
#RUN conda install -n base -c conda-forge widgetsnbextension
#RUN conda install -n rapids -c conda-forge widgetsnbextension


#RUN conda install -n base -c conda-forge nodejs
#RUN conda install -n base -c conda-forge jupyterlab
#RUN conda install -n rapids -c conda-forge nodejs
#RUN conda install -n rapids -c conda-forge jupyterlab
#RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager ipycanvas
#RUN conda run -n rapids jupyter lab build

#RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager ipycanvas
#RUN jupyter labextension list
#RUN jupyter serverextension enable --py jupyterlab --sys-prefix

#RUN conda install -c conda-forge nodejs

#RUN pip install ipywidgets jupyterlab \
#  && jupyter nbextension enable --py widgetsnbextension --sys-prefix \
#  && jupyter labextension install @jupyter-widgets/jupyterlab-manager ipycanvas
# adding ipycanvas

#RUN jupyter nbextension enable --py --sys-prefix widgetsnbextension 
#RUN jupyter labextension install ipycanvas

# Create working directory to add repo.
WORKDIR /workshop

#
#RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager 


#RUN source activate rapids \ 
#    && conda install -y -c conda-forge dask-labextension \
#     nodejs=11.11.0 jupyterlab=0.35.4 ipywidgets=7.4.2 ipycanvas

#RUN source activate rapids \ 
#    && jupyter labextension install @jupyter-widgets/jupyterlab-manager@0.38.1 ipycanvas  --no-build \
#    && mkdir /.local /.jupyter /.config /.cupy  \
#    && chmod 777 /.local /.jupyter /.config /.cupy

RUN source activate rapids \ 
    && conda install -y -c conda-forge dask-labextension \
     nodejs jupyterlab ipywidgets ipycanvas

RUN source activate rapids \ 
    && jupyter labextension install @jupyter-widgets/jupyterlab-manager ipycanvas  --no-build 

RUN conda install -c conda-forge jupyter_contrib_nbextensions

RUN source activate rapids \ 
    && jupyter lab build && jupyter lab clean

RUN apt-get update -q && apt-get install --no-install-recommends -yq nvidia-cuda-toolkit

RUN apt-get install --no-install-recommends -y   build-essential git make gcc g++ libgles2-mesa-dev libsdl2-dev ltrace
#RUN apt-get update
#RUN apt-get -y upgrade
#RUN apt-get install -y nvidia-cuda-toolkit

# Load contents into student working directory.
ADD . .

# Create working directory for students.
WORKDIR /workshop/content

# Jupyter listens on 8888.
EXPOSE 8888

# Please see `entrypoint.sh` for details on how this content
# is launched.
ADD entrypoint.sh /usr/local/bin
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
