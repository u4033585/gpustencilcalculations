# GTC GPU Stencil Calculations Repo



This repo gives provides the content and example code for the GPU stencil calculation lab. 

# Docker Instructions

Use `Dockerfile` to build a docker image with `docker build -t gtc_stencil_repo .`.

Run a container from this image with `docker run -p 8888:8888 --runtime=nvidia gtc_stencil_repo`.

Once the container is running, visit the content in your browser at `http://localhost:8888`.


To run docker so that content files can be edited one can mount the directory within the container with:
`docker run -v /home/ericm/courses/gpu/GPUStencilCalculations:/workshop -p 8888:8888 --runtime=nvidia gtc_stencil_repo`   (you will need to set /home/ericm/courses/gpu/GPUStencilCalculations as the absolute path to the directory).  Note this requires the docker build for modifications to be reflected.
